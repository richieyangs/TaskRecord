﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TaskRecord.Web.EntityFramework;

namespace TaskRecord.Web.Dal
{
    public class TaskRecordSourceDal:IDal
    {
        public List<TaskRecordSource> GetTaskRecordSources()
        {
            using (var db = new TaskRecordDbContext())
            {
                var records = db.TaskRecordSources.ToList();
                return records;
            }
        }

        public void AddTaskRecordSources(TaskRecordSource source)
        {
            using (var db = new TaskRecordDbContext())
            {
                db.TaskRecordSources.Add(source);
                db.SaveChanges();
            }
        }
    }
}