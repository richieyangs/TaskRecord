﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TaskRecord.Web.EntityFramework
{
    public class TaskRecordConfiguration : EntityTypeConfiguration<TaskRecord>
    {
        public TaskRecordConfiguration()
        {
            HasKey(m => m.Id);

            Property(m => m.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(m => m.TimeStamp).IsRowVersion();

        }
    }

    public class TeamRecordConfiguration : EntityTypeConfiguration<TeamRecord>
    {
        public TeamRecordConfiguration()
        {
            HasKey(m => m.Id);

            Property(m => m.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(m => m.TimeStamp).IsRowVersion();

        }
    } 
    
    public class NoteRecordConfiguration : EntityTypeConfiguration<NoteRecord>
    {
        public NoteRecordConfiguration()
        {
            HasKey(m => m.Id);

            Property(m => m.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(m => m.TimeStamp).IsRowVersion();

        }
    }

    public class TaskRecordTeamConfiguration : EntityTypeConfiguration<TaskRecordTeam>
    {
        public TaskRecordTeamConfiguration()
        {
            HasKey(m => m.Id);

            Property(m => m.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
                .IsRequired();
            Property(m => m.Name)
                .IsRequired();

            HasMany(m => m.TaskRecords)
                .WithRequired(p => p.Team);

            Property(m => m.TimeStamp).IsRowVersion();
        }

       
    }

    public class TaskRecordSourceConfiguration : EntityTypeConfiguration<TaskRecordSource>
    {
        public TaskRecordSourceConfiguration()
        {
            HasKey(m => m.Id);

            Property(m => m.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
                .IsRequired();
            Property(m => m.Name)
                .IsRequired();

            HasMany(m => m.TaskRecords)
               .WithRequired(p => p.Source);

            Property(m => m.TimeStamp).IsRowVersion();
        }
    }

    public class TaskRecordStatusConfiguration : EntityTypeConfiguration<TaskRecordStatus>
    {
        public TaskRecordStatusConfiguration()
        {
            HasKey(m => m.Id);

            Property(m => m.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None)
                .IsRequired();
            Property(m => m.Name)
                .IsRequired();

            HasMany(m => m.TaskRecords)
             .WithRequired(p => p.Status);

            Property(m => m.TimeStamp).IsRowVersion();
        }
    }
}