﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Http.Routing;

namespace TaskRecord.Web.TypedLink
{
    public static class TypedLinkExtensions
    {
        public static string Action<TController, TResult>(this UrlHelper @this,
            Expression<Func<TController, TResult>> lambdaExpression)
        {
            var routeModel = RouteValuesExtractor.ExtractRouteValues(lambdaExpression);
            return @this.Link(routeModel.RouteName, routeModel.RouteValuesDictionary);
        }
    }
}