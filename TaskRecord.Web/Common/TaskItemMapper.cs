﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace TaskRecord.Web.Common
{
    public static class TaskItemMapper
    {
        public static Guid TeamOneId { get { return Guid.Parse("B38750EA-508F-460D-8A74-5D2BE8D78CFB"); } }
        public static Guid TeamTwoId { get { return Guid.Parse("75998AA8-ED1E-4AD7-B3E7-695EE8864A61"); } }
        public static Guid TeamThreeId { get { return Guid.Parse("F49A5BDB-6D48-481D-B1A6-12BB0B6C8342"); } }
        public static Guid TeamFourId { get { return Guid.Parse("2A8EE9AA-0A7B-4900-AC43-640E0E28C7E8"); } }
        public static Guid SourceOfRunningId { get { return Guid.Parse("575A83DB-EC46-4AE8-ACBA-10B18B76067D"); } }
        public static Guid SourceOfProduceId { get { return Guid.Parse("D8D66630-71D2-4A33-8C1D-C7EC7436649B"); } }
        public static Guid StatusOfComplete { get { return Guid.Parse("BC73D08F-7B6E-4991-9968-D42EEF4598E0"); } }
        public static Guid StatusOfUnComplete { get { return Guid.Parse("287BEAEB-260F-4C7B-B7CF-D8060CCC418E"); } }
        public static IReadOnlyDictionary<Guid, string> TeamsMapper { get { return _teamsMapper.AsReadOnly(); } }
        public static IReadOnlyDictionary<Guid, string> SourcesMapper { get { return _sourcesMapper.AsReadOnly(); } }
        public static IReadOnlyDictionary<Guid, string> StatusMapper { get { return _statusMapper.AsReadOnly(); } }
        // ReSharper disable once InconsistentNaming
        private static readonly Dictionary<Guid, string> _teamsMapper;
        // ReSharper disable once InconsistentNaming
        private static readonly Dictionary<Guid, string> _sourcesMapper;
        // ReSharper disable once InconsistentNaming
        private static readonly Dictionary<Guid, string> _statusMapper;

        static TaskItemMapper()
        {
            _teamsMapper = new Dictionary<Guid, string>
            {
                {Guid.Parse("B38750EA-508F-460D-8A74-5D2BE8D78CFB"), "电一"},
                {Guid.Parse("75998AA8-ED1E-4AD7-B3E7-695EE8864A61"), "电二"},
                {Guid.Parse("F49A5BDB-6D48-481D-B1A6-12BB0B6C8342"), "电三"},
                {Guid.Parse("2A8EE9AA-0A7B-4900-AC43-640E0E28C7E8"), "电四"}
            };

            _sourcesMapper = new Dictionary<Guid, string>()
            {
                {Guid.Parse("575A83DB-EC46-4AE8-ACBA-10B18B76067D"),"运行调度"},
                {Guid.Parse("D8D66630-71D2-4A33-8C1D-C7EC7436649B"),"生产调度"},

            };

            _statusMapper = new Dictionary<Guid, string>()
            {
                {Guid.Parse("BC73D08F-7B6E-4991-9968-D42EEF4598E0"),"完成"},
                {Guid.Parse("287BEAEB-260F-4C7B-B7CF-D8060CCC418E"),"未完"}
            };

        }

        public static string GetStatusName(Guid id)
        {
            return _statusMapper[id];
        }

        public static string GetSourceName(Guid id)
        {
            return _sourcesMapper[id];
        }

        public static string GetTeamName(Guid id)
        {
            return _teamsMapper[id];
        }

        public static Guid GetTeamId(string name)
        {
            return _teamsMapper.Single(x => x.Value == name).Key;
        }

        public static Guid GetSourceId(string name)
        {
            return _sourcesMapper.Single(x => x.Value == name).Key;
        }

        public static Guid GetStatusId(string name)
        {
            return _statusMapper.Single(x => x.Value == name).Key;
        }

        public static ReadOnlyDictionary<TKey, TValue> AsReadOnly<TKey, TValue>(
            this IDictionary<TKey, TValue> dictionary)
        {
            return new ReadOnlyDictionary<TKey, TValue>(dictionary);
        }
    }
}