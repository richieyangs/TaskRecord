﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TaskRecord.Web.Common;
using TaskRecord.Web.EntityFramework;

namespace TaskRecord.Web.Dal
{
    public class DictionaryDataOperator
    {
        public static void AddIfNotExsist()
        {
            var teamDal = new TaskRecordTeamDal();
            if (!teamDal.GetTaskRecordTeams().Any())
            {
                teamDal.AddTaskRecordTeams(new TaskRecordTeam() { Id = TaskItemMapper.TeamOneId, Name = TaskItemMapper.GetTeamName(TaskItemMapper.TeamOneId) });
                teamDal.AddTaskRecordTeams(new TaskRecordTeam() { Id = TaskItemMapper.TeamTwoId, Name = TaskItemMapper.GetTeamName(TaskItemMapper.TeamTwoId) });
                teamDal.AddTaskRecordTeams(new TaskRecordTeam() { Id = TaskItemMapper.TeamThreeId, Name = TaskItemMapper.GetTeamName(TaskItemMapper.TeamThreeId) });
                teamDal.AddTaskRecordTeams(new TaskRecordTeam() { Id = TaskItemMapper.TeamFourId, Name = TaskItemMapper.GetTeamName(TaskItemMapper.TeamFourId) });
            }

            var sourceDal=new TaskRecordSourceDal();
            if (!sourceDal.GetTaskRecordSources().Any())
            {
                sourceDal.AddTaskRecordSources(new TaskRecordSource(){Id = TaskItemMapper.SourceOfProduceId,Name = TaskItemMapper.GetSourceName(TaskItemMapper.SourceOfProduceId)});
                sourceDal.AddTaskRecordSources(new TaskRecordSource() { Id = TaskItemMapper.SourceOfRunningId, Name = TaskItemMapper.GetSourceName(TaskItemMapper.SourceOfRunningId) });
            }

            var statusDal=new TaskRecordStatusDal();
            if (!statusDal.GetTaskRecordStatus().Any())
            {
                statusDal.AddTaskRecordStatus(new TaskRecordStatus(){Id = TaskItemMapper.StatusOfComplete,Name = TaskItemMapper.GetStatusName(TaskItemMapper.StatusOfComplete)});
                statusDal.AddTaskRecordStatus(new TaskRecordStatus() { Id = TaskItemMapper.StatusOfUnComplete, Name = TaskItemMapper.GetStatusName(TaskItemMapper.StatusOfUnComplete) });
            }
        }

    }
}