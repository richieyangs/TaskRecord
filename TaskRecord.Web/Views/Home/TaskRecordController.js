﻿angular.module("taskRecordApp").controller("taskRecordController", ["$scope", "requestService", "editConditionService", "apiNavigator", "bindCorrectOptionService", function ($scope, requestService, editConditionService, apiNavigator, bindCorrectOptionService) {
    $scope.saving = true;
    $scope.loadTaskRecords = function (date) {
        var queryDate = { queryDate: date };

        apiNavigator.getHomeResource()
            .then(function(resources) {
                requestService.postRequest(queryDate, resources.taskRecords)
                    .then(function(data) {
                        $scope.taskRecordsResource = data;
                        $scope.canEdit = editConditionService.canEdit(date);

                        bindCorrectOptionService.bind($scope.taskRecordsResource);
                       
                        $scope.saving = false;
                    });
            });


    };

    $scope.loadTaskRecords(new Date());

    /****************datepicker***************************************/
    $scope.query = function () {
        var queryDate = $scope.taskRecordsResource.queryDate;

        $scope.loadTaskRecords(queryDate);

    };

    $scope.setDateRange = function () {
        $scope.minDate = null;
        $scope.maxDate = new Date();
    };
    $scope.open = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.opened = true;
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.setDateRange();
}]);

