﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace TaskRecord.Web.Views.Api
{
    public class TaskRecordApiController:ApiController
    {
        [HttpGet,Route("api/index",Name = "815B46FE-3D7B-43B6-8D9C-F35C1DEACA2F")]
        public TaskRecordsApiLinks Home()
        {
            return new TaskRecordsApiLinks(Url);
        }
    }
}