﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using TaskRecord.Web.Common;
using TaskRecord.Web.Dal;

namespace TaskRecord.Web.Views.Home.Api
{
    public class HomeController : ApiController
    {
        private readonly TaskRecordDal _taskRecordDal;
        private readonly TeamRecordDal _teamRecordDal;
        private readonly NoteRecordDal _noteRecordDal;
        private readonly TaskRecordTeamDal _taskRecordTeamDal;
        private readonly TaskRecordSourceDal _taskRecordSourceDal;
        private readonly TaskRecordStatusDal _taskRecordStatusDal;

        public HomeController(TaskRecordDal taskRecordDal,
            TeamRecordDal teamRecordDal,
            NoteRecordDal noteRecordDal,
            TaskRecordTeamDal taskRecordTeamDal,
            TaskRecordSourceDal taskRecordSourceDal,
            TaskRecordStatusDal taskRecordStatusDal
            )
        {
            _taskRecordDal = taskRecordDal;
            _teamRecordDal = teamRecordDal;
            _noteRecordDal = noteRecordDal;
            _taskRecordTeamDal = taskRecordTeamDal;
            _taskRecordSourceDal = taskRecordSourceDal;
            _taskRecordStatusDal = taskRecordStatusDal;
        }

        [HttpPost, Route("api/taskrecords", Name = "BFD311B9-C6E0-4A64-A411-97BB9DAF5876")]
        public TaskViewModel GetTaskRecords(TaskRecordsQuery taskRecordsQuery)
        {
            var date = taskRecordsQuery == null ? DateTime.Now : taskRecordsQuery.QueryDate;
            var taskRecords = _taskRecordDal.GetTaskRecords(date);
            var model = new TaskViewModel(Url)
            {
                QueryDate = date,
                TeamOneTaskRecords = taskRecords.Where(x => x.Team.Id == TaskItemMapper.TeamOneId).ToViewModels(),
                TeamTwoTaskRecords = taskRecords.Where(x => x.Team.Id == TaskItemMapper.TeamTwoId).ToViewModels(),
                TeamThreeTaskRecords = taskRecords.Where(x => x.Team.Id == TaskItemMapper.TeamThreeId).ToViewModels(),
                TeamFourTaskRecords = taskRecords.Where(x => x.Team.Id == TaskItemMapper.TeamFourId).ToViewModels(),
                TeamRecordViewModel = _teamRecordDal.GetTeamRecord(date).ToViewModel(),
                Notes = _noteRecordDal.GetNoteRecords(date).ToViewModels()
            };

            var teams = _taskRecordTeamDal.GetTaskRecordTeams();
            var status = _taskRecordStatusDal.GetTaskRecordStatus();
            var source = _taskRecordSourceDal.GetTaskRecordSources();

            model.TaskRecordStatus = status.Select(x=>x.ToViewModel()).ToList();
            model.TaskRecordSources = source.Select(x => x.ToViewModel()).ToList();

            model.TeamOneNewTaskRecord=new TaskRecordViewModel()
            {
                Team = teams.Single(t=>t.Id==TaskItemMapper.TeamOneId).ToViewModel(),
            }; 
            
            model.TeamTwoNewTaskRecord=new TaskRecordViewModel()
            {
                Team = teams.Single(t=>t.Id==TaskItemMapper.TeamTwoId).ToViewModel(),
            };  
            model.TeamThreeNewTaskRecord=new TaskRecordViewModel()
            {
                Team = teams.Single(t=>t.Id==TaskItemMapper.TeamThreeId).ToViewModel(),
            };
            
            model.TeamFourNewTaskRecord=new TaskRecordViewModel()
            {
                Team = teams.Single(t=>t.Id==TaskItemMapper.TeamFourId).ToViewModel(),
            };

            return model;
        }

        [HttpPost, Route("api/teamrecord/save", Name = "3A3BAEC4-2504-4647-ACB1-FA830004A5B5")]
        public TaskViewModel SaveTeamRecord(TeamRecordViewModel record)
        {
            if (record.Id == Guid.Empty)
            {
                _teamRecordDal.AddTeamRecord(record.ToData());
            }
            else
            {
                _teamRecordDal.EditTeamRecord(record.ToData());
            }
            return GetTaskRecords(null);
        }

        [HttpPost, Route("api/noterecord/add", Name = "1D8AA4C1-D56A-46A2-82D6-BB0F862262D0")]
        public TaskViewModel AddNoteRecord(NoteRecordViewModel record)
        {
            _noteRecordDal.AddNoteRecord(record.ToData());
            return GetTaskRecords(null);
        }

        [HttpPost, Route("api/noterecord/edit", Name = "B8EB98C6-8CF3-41EF-884B-A7BD2AF23762")]
        public TaskViewModel EditNoteRecord(NoteRecordViewModel record)
        {
            _noteRecordDal.EditNoteRecord(record.ToData());
            return GetTaskRecords(null);
        }

        [HttpPost, Route("api/taskrecord/add", Name = "4E1DB155-1B56-448E-8C38-D1F594486E0D")]
        public TaskViewModel AddTaskRecord(TaskRecordViewModel record)
        {
            var team = record.Team.ToData();
            var source = record.Source.ToData();
            var status = record.Status.ToData();
            _taskRecordDal.AddTaskRecord(record.ToData());

            return GetTaskRecords(null);
        }

        [HttpPost, Route("api/taskrecord/update", Name = "5E70A99D-C8CE-4D86-8C7C-74D37639C990")]
        public TaskViewModel UpdateTaskRecord(TaskRecordViewModel record)
        {
            _taskRecordDal.EditTaskRecord(record.ToData());

            return GetTaskRecords(null);
        }

        [HttpGet, Route("api/taskreordteam", Name = "E2D9A3D1-2B4C-44E7-A7ED-77D5D4CB9543")]
        public List<TaskRecordTeamViewModel> TaskRecordTeams()
        {
            return _taskRecordTeamDal.GetTaskRecordTeams().Select(x => x.ToViewModel()).ToList();
        }

        [HttpGet, Route("api/taskreordstatus", Name = "CD767CAB-A701-4100-A970-E831A90F8CBC")]
        public List<TaskRecordStatusViewModel> TaskRecordStatus()
        {
            return _taskRecordStatusDal.GetTaskRecordStatus().Select(x => x.ToViewModel()).ToList();
        }

        [HttpGet, Route("api/taskrecordsource", Name = "0CDD5394-B0C8-4148-BA7C-9F70080A4E4A")]
        public List<TaskRecordSourceViewModel> TaskRecordSources()
        {
            return _taskRecordSourceDal.GetTaskRecordSources().Select(x => x.ToViewModel()).ToList();
        }

    }
}