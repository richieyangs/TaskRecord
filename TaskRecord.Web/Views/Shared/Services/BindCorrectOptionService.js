﻿angular.module("taskRecordApp")
    .service("bindCorrectOptionService", function() {
        this.bind = function(taskRecordsResource) {
            taskRecordsResource.teamOneNewTaskRecord.source =taskRecordsResource.taskRecordSources[0];
            taskRecordsResource.teamOneNewTaskRecord.status =taskRecordsResource.taskRecordStatus[0];

           taskRecordsResource.teamTwoNewTaskRecord.source =taskRecordsResource.taskRecordSources[0];
           taskRecordsResource.teamTwoNewTaskRecord.status =taskRecordsResource.taskRecordStatus[0];

            taskRecordsResource.teamThreeNewTaskRecord.source =taskRecordsResource.taskRecordSources[0];
            taskRecordsResource.teamThreeNewTaskRecord.status =taskRecordsResource.taskRecordStatus[0];

            taskRecordsResource.teamFourNewTaskRecord.source = taskRecordsResource.taskRecordSources[0];
            taskRecordsResource.teamFourNewTaskRecord.status = taskRecordsResource.taskRecordStatus[0];

            angular.forEach(taskRecordsResource.teamOneTaskRecords, function (item) {
                item.source = taskRecordsResource.taskRecordSources.filter(function (source) {
                    return source.id === item.source.id;
                })[0];

                item.status = taskRecordsResource.taskRecordStatus.filter(function (status) {
                    return status.id === item.status.id;
                })[0];
            });

            angular.forEach(taskRecordsResource.teamTwoTaskRecords, function (item) {
                item.source = taskRecordsResource.taskRecordSources.filter(function (source) {
                    return source.id === item.source.id;
                })[0];

                item.status = taskRecordsResource.taskRecordStatus.filter(function (status) {
                    return status.id === item.status.id;
                })[0];
            });

            angular.forEach(taskRecordsResource.teamThreeTaskRecords, function (item) {
                item.source = taskRecordsResource.taskRecordSources.filter(function (source) {
                    return source.id === item.source.id;
                })[0];

                item.status =taskRecordsResource.taskRecordStatus.filter(function (status) {
                    return status.id === item.status.id;
                })[0];
            });

            angular.forEach(taskRecordsResource.teamFourTaskRecords, function (item) {
                item.source =taskRecordsResource.taskRecordSources.filter(function (source) {
                    return source.id === item.source.id;
                })[0];

                item.status = taskRecordsResource.taskRecordStatus.filter(function (status) {
                    return status.id === item.status.id;
                })[0];
            });
        };

    });