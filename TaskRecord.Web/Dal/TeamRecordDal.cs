﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using TaskRecord.Web.EntityFramework;

namespace TaskRecord.Web.Dal
{
    public class TeamRecordDal:IDal
    {
        public TeamRecord GetTeamRecord(DateTime queryDate)
        {

            var queryTomorrow = queryDate.Date.AddDays(1);
            var queryYestoday = queryDate.Date;
            using (var db = new TaskRecordDbContext())
            {
                var records = db.Teams.Where(x => (x.Date >= queryYestoday && x.Date <= queryTomorrow)).ToList();
                return records.FirstOrDefault();
            }
        }

        public void EditTeamRecord(EntityFramework.TeamRecord teamRecord)
        {
            using (var db = new TaskRecordDbContext())
            {
                var record = db.Teams.Single(x => x.Id == teamRecord.Id);
                record.Operator = teamRecord.Operator;
                record.AssistantOperator = teamRecord.AssistantOperator;
                record.NightOperator = teamRecord.NightOperator;
                record.ExternalOperator = teamRecord.ExternalOperator;
                record.RepaireMan = teamRecord.RepaireMan;
                record.ProduceOffice = teamRecord.ProduceOffice;
                record.CompositeOffice = teamRecord.CompositeOffice;
                record.ManageOffice = teamRecord.ManageOffice;

                db.Entry(record).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void AddTeamRecord(EntityFramework.TeamRecord teamRecord)
        {
            teamRecord.Date = DateTime.Now;
            using (var db = new TaskRecordDbContext())
            {
                db.Teams.Add(teamRecord);
                db.SaveChanges();
            }
        }
    }
}