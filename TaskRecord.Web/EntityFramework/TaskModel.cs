﻿using System;
using System.Collections.Generic;

namespace TaskRecord.Web.EntityFramework
{
    public class TaskRecord
    {
        public Guid Id { get; set; }
        public DateTime Date { get; set; }
        public string Content { get; set; }
        public string Car { get; set; }

        public virtual TaskRecordTeam Team { get; set; }
        public virtual TaskRecordSource Source { get; set; }
        public virtual TaskRecordStatus Status { get; set; }
        public byte[] TimeStamp { get; set; }

    }

    public class TeamRecord
    {
        public Guid Id { get; set; }
        public DateTime Date { get; set; }
        public string Operator { get; set; }
        public string AssistantOperator { get; set; }
        public string NightOperator { get; set; }
        public string ExternalOperator { get; set; }
        public string RepaireMan { get; set; }
        public string ProduceOffice { get; set; }
        public string CompositeOffice { get; set; }
        public string ManageOffice { get; set; }
        public byte[] TimeStamp { get; set; }
    }

    public class NoteRecord
    {
        public Guid Id { get; set; }
        public DateTime Date { get; set; }
        public string Content { get; set; }
        public byte[] TimeStamp { get; set; }
    }

    public class TaskRecordTeam
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public virtual List<TaskRecord> TaskRecords { get; set; } 
        public byte[] TimeStamp { get; set; }
    }

    public class TaskRecordSource
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public virtual List<TaskRecord> TaskRecords { get; set; } 
        public byte[] TimeStamp { get; set; }
    }

    public class TaskRecordStatus
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public virtual List<TaskRecord> TaskRecords { get; set; } 
        public byte[] TimeStamp { get; set; }
    }
}