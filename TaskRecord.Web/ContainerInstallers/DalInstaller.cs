﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using TaskRecord.Web.Dal;

namespace TaskRecord.Web.ContainerInstallers
{
    public class DalInstaller:IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromThisAssembly().BasedOn<IDal>().WithService.Self().LifestylePerWebRequest());
        }
    }
}