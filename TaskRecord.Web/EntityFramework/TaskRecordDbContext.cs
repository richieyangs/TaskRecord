﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace TaskRecord.Web.EntityFramework
{
    public class TaskRecordDbContext:DbContext
    {
        public const string ConnectionString = "TaskRecords";

           public TaskRecordDbContext()
            : base(ConnectionString)
        {
            Configuration.AutoDetectChangesEnabled = true;
            Configuration.LazyLoadingEnabled = true;
        }

        override protected void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<TaskRecord>().Map(m => m.MapInheritedProperties());
            modelBuilder.Entity<TeamRecord>().Map(m => m.MapInheritedProperties());
            modelBuilder.Entity<NoteRecord>().Map(m => m.MapInheritedProperties());
            modelBuilder.Entity<TaskRecordSource>().Map(m => m.MapInheritedProperties());
            modelBuilder.Entity<TaskRecordTeam>().Map(m => m.MapInheritedProperties());
            modelBuilder.Entity<TaskRecordStatus>().Map(m => m.MapInheritedProperties());

            modelBuilder.Configurations
                .Add(new TaskRecordConfiguration())
                .Add(new TeamRecordConfiguration())
                .Add(new NoteRecordConfiguration())
                .Add(new TaskRecordTeamConfiguration())
                .Add(new TaskRecordSourceConfiguration())
                .Add(new TaskRecordStatusConfiguration())
                ;
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>(); 
            base.OnModelCreating(modelBuilder);
        }

        public DbSet<TaskRecord> Tasks { get; set; } 
        public DbSet<TeamRecord> Teams { get; set; } 
        public DbSet<NoteRecord> Notes { get; set; }
        public DbSet<TaskRecordTeam> TaskRecordTeams { get; set; }
        public DbSet<TaskRecordSource> TaskRecordSources { get; set; } 
        public DbSet<TaskRecordStatus> TaskRecordStatus { get; set; } 

    }
}