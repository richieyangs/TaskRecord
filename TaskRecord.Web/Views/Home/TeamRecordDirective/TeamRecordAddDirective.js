﻿angular.module("taskRecordApp")
    .directive("teamRecordAdd", [
        "requestService", "editConditionService", "bindCorrectOptionService", function (requestService, editConditionService, bindCorrectOptionService) {

            return {
                restrict: "E",
                templateUrl: "Views/Home/TeamRecordDirective/TeamRecordAddTemplate.html",
                scope: {
                    taskRecordsResource: "=",
                    teamRecord: "=",
                    saving: "=",
                    canEdit:"="

                },
                link: function (scope, element, attrs, ctrl) {
                    scope.editing = function (input) {
                        if (scope.canEdit) {
                            scope[input] = true;
                        }
                    };

                    scope.saveTeamRecord = function () {
                        var resources = scope.taskRecordsResource.apiLinks;

                        if (!scope.canEdit) {
                            return;
                        }
                        scope.saving = true;
                        requestService.postRequest(scope.teamRecord, resources.saveTeamRecord)
                            .then(function (data) {
                                scope.taskRecordsResource = data;
                                bindCorrectOptionService.bind(scope.taskRecordsResource);
                                scope.saving = false;
                            });
                    }
                }
            }
        }
    ]);