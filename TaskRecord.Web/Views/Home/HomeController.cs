﻿using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using TaskRecord.Web.Common;
using TaskRecord.Web.Dal;

namespace TaskRecord.Web.Views.Home
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var model = new TaskViewModel()
            {
                QueryDate = DateTime.Now,
            };


            return View(model);
        }

    }
}