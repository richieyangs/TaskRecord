﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TaskRecord.Web.EntityFramework;

namespace TaskRecord.Web.Dal
{
    public class TaskRecordTeamDal:IDal
    {
        public List<TaskRecordTeam> GetTaskRecordTeams()
        {
            using (var db = new TaskRecordDbContext())
            {
                var records = db.TaskRecordTeams.ToList();
                return records;
            }
        }

        public void AddTaskRecordTeams(TaskRecordTeam team)
        {
            using (var db = new TaskRecordDbContext())
            {
                db.TaskRecordTeams.Add(team);
                db.SaveChanges();
            }
        }
    }
}