using System;
using System.Web.Http.Routing;
using TaskRecord.Web.TypedLink;
using TaskRecord.Web.Views.Home.Api;

namespace TaskRecord.Web.Views.Api
{
    public class TaskRecordsApiLinks
    {
        public TaskRecordsApiLinks(UrlHelper url)
        {
            TaskRecords = url.Action((HomeController c) => c.GetTaskRecords(null));
            SaveTeamRecord = url.Action((HomeController c) => c.SaveTeamRecord(null));
            AddNoteRecord = url.Action((HomeController c) => c.AddNoteRecord(null));
            EditNoteRecord = url.Action((HomeController c) => c.EditNoteRecord(null));
            AddTaskRecord = url.Action((HomeController c) => c.AddTaskRecord(null));
            UpdateTaskRecord = url.Action((HomeController c) => c.UpdateTaskRecord(null));

            TaskRecordTeam = url.Action((HomeController c) => c.TaskRecordTeams());
            TaskRecordSource = url.Action((HomeController c) => c.TaskRecordSources());
            TaskRecordStatus = url.Action((HomeController c) => c.TaskRecordStatus());
        }
        public string TaskRecords { get; set; }
        public string SaveTeamRecord { get; set; }
        public string AddNoteRecord { get; set; }
        public string EditNoteRecord { get; set; }
        public string AddTaskRecord { get; set; }
        public string UpdateTaskRecord { get; set; }

        public string TaskRecordTeam { get; set; }
        public string TaskRecordSource { get; set; }
        public string TaskRecordStatus { get; set; }
    }
}