﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TaskRecord.Web.EntityFramework;

namespace TaskRecord.Web.Dal
{
    public class TaskRecordStatusDal:IDal
    {
        public List<TaskRecordStatus> GetTaskRecordStatus()
        {
            using (var db = new TaskRecordDbContext())
            {
                var records = db.TaskRecordStatus.ToList();
                return records;
            }
        }

        public void AddTaskRecordStatus(TaskRecordStatus statusDal)
        {
            using (var db = new TaskRecordDbContext())
            {
                db.TaskRecordStatus.Add(statusDal);
                db.SaveChanges();
            }
        }
    }
}