﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using System.Web.Http.Routing;
using TaskRecord.Web.Common;
using TaskRecord.Web.Views.Api;

namespace TaskRecord.Web.Views.Home
{
    public class TaskViewModel
    {
        public DateTime Date { get; set; }
        public List<TaskRecordViewModel> TeamOneTaskRecords { get; set; }
        public List<TaskRecordViewModel> TeamTwoTaskRecords { get; set; }
        public List<TaskRecordViewModel> TeamThreeTaskRecords { get; set; }
        public List<TaskRecordViewModel> TeamFourTaskRecords { get; set; }
        public TeamRecordViewModel TeamRecordViewModel { get; set; }
        public List<NoteRecordViewModel> Notes { get; set; }
        public NoteRecordViewModel NewNoteRecord { get; set; }
        public TaskRecordViewModel TeamOneNewTaskRecord { get; set; }
        public TaskRecordViewModel TeamTwoNewTaskRecord { get; set; }
        public TaskRecordViewModel TeamThreeNewTaskRecord { get; set; }
        public TaskRecordViewModel TeamFourNewTaskRecord { get; set; }

        public List<TaskRecordSourceViewModel> TaskRecordSources { get; set; } 
        public List<TaskRecordStatusViewModel> TaskRecordStatus { get; set; } 

        public DateTime QueryDate { get; set; }
        public TaskRecordsApiLinks ApiLinks { get; set; }

        public TaskViewModel()
        {
            
        }

        public TaskViewModel(UrlHelper url)
        {
            Date=DateTime.Now;
            TeamOneTaskRecords = new List<TaskRecordViewModel>();
            TeamTwoTaskRecords = new List<TaskRecordViewModel>();
            TeamThreeTaskRecords = new List<TaskRecordViewModel>();
            TeamFourTaskRecords = new List<TaskRecordViewModel>();
            TeamRecordViewModel = new TeamRecordViewModel();
            Notes = new List<NoteRecordViewModel>();
            NewNoteRecord = new NoteRecordViewModel();

            ApiLinks=new TaskRecordsApiLinks(url);
        }
    }

    public class TaskRecordViewModel
    {
        public Guid Id { get; set; }
        
        public string Content { get; set; }
        public TaskRecordTeamViewModel Team { get; set; }
        public TaskRecordSourceViewModel Source { get; set; }
        public TaskRecordStatusViewModel Status { get; set; }
        public string Car { get; set; }

    }

    public class TeamRecordViewModel
    {
        public Guid Id { get; set; }
        public string Operator { get; set; }
        public string AssistantOperator { get; set; }
        public string NightOperator { get; set; }
        public string ExternalOperator { get; set; }
        public string RepaireMan { get; set; }
        public string ProduceOffice { get; set; }
        public string CompositeOffice { get; set; }
        public string ManageOffice { get; set; }
    }

    public class NoteRecordViewModel
    {
        public Guid Id { get; set; }
        public string Content { get; set; }
    }

    public class TaskRecordTeamViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }

    public class TaskRecordSourceViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }

    public class TaskRecordStatusViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
   
}