﻿var app = angular.module("taskRecordApp", ["ngSanitize", "ui.bootstrap", "angularSpinner", "ui.router"]);

app.config(['usSpinnerConfigProvider', function (usSpinnerConfigProvider) {
    usSpinnerConfigProvider.setDefaults({ color: 'orange' });
}])
.config(["apiNavigatorProvider", function (apiNavigatorProvider) {
    apiNavigatorProvider.setApiHome("api/index");
}])
;