﻿using Castle.Windsor;
using Castle.Windsor.Installer;

namespace TaskRecord.Web
{
    public class ApplicationBootstrap
    {
        public static IWindsorContainer SetupContainer()
        {
            var container = new WindsorContainer();

            container.Install(FromAssembly.This());

            return container;
        } 
    }
}