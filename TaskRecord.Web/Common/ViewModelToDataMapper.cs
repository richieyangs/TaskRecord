﻿using AutoMapper;
using TaskRecord.Web.EntityFramework;
using TaskRecord.Web.Views.Home;

namespace TaskRecord.Web.Common
{
    public static class ViewModelToDataMapper
    {
        private static IMappingEngine _engine;

        static ViewModelToDataMapper()
        {
            _engine = MappingEnginer.BuildEngine(configuration =>
            {
                configuration.CreateMap<TaskRecordViewModel, EntityFramework.TaskRecord>()
                    .ForMember(d => d.Team, s => s.MapFrom(m => m.Team.ToData()))
                    .ForMember(d => d.Source, s => s.MapFrom(m => m.Source.ToData()))
                    .ForMember(d => d.Status, s => s.MapFrom(m => m.Status.ToData()))
                    .ForMember(d => d.Date, s => s.Ignore())
                    .ForMember(d => d.TimeStamp, s => s.Ignore())
                    ;

                configuration.CreateMap<TeamRecordViewModel, TeamRecord>()
                    .ForMember(d => d.Date, s => s.Ignore())
                    .ForMember(d => d.TimeStamp, s => s.Ignore());

                configuration.CreateMap<NoteRecordViewModel, NoteRecord>()
                    .ForMember(d => d.Date, s => s.Ignore())
                    .ForMember(d => d.TimeStamp, s => s.Ignore())
                    ;

                configuration.CreateMap<TaskRecordTeamViewModel, TaskRecordTeam>()
                    .ForMember(d => d.TaskRecords, s => s.Ignore())
                    .ForMember(d => d.TimeStamp, s => s.Ignore())
                    ;

                configuration.CreateMap<TaskRecordSourceViewModel, TaskRecordSource>()
                    .ForMember(d => d.TaskRecords, s => s.Ignore())
                    .ForMember(d => d.TimeStamp, s => s.Ignore())
                    ;

                configuration.CreateMap<TaskRecordStatusViewModel, TaskRecordStatus>()
                    .ForMember(d => d.TaskRecords, s => s.Ignore())
                    .ForMember(d => d.TimeStamp, s => s.Ignore())
                    ;
            });
        }

        public static EntityFramework.TaskRecord ToData(this TaskRecordViewModel taskRecord)
        {
            return _engine.Map<EntityFramework.TaskRecord>(taskRecord);
        }

        public static TeamRecord ToData(this TeamRecordViewModel teamRecord)
        {
            return _engine.Map<TeamRecord>(teamRecord);
        }

        public static NoteRecord ToData(this NoteRecordViewModel noteRecord)
        {
            return _engine.Map<NoteRecord>(noteRecord);
        }

        public static TaskRecordTeam ToData(this TaskRecordTeamViewModel team)
        {
            return _engine.Map<TaskRecordTeam>(team);
        }

        public static TaskRecordSource ToData(this TaskRecordSourceViewModel source)
        {
            return _engine.Map<TaskRecordSource>(source);
        }

        public static TaskRecordStatus ToData(this TaskRecordStatusViewModel source)
        {
            return _engine.Map<TaskRecordStatus>(source);
        }
    }
}