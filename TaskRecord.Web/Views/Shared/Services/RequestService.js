﻿angular.module("taskRecordApp")
    .service("requestService", ["$q", "$http", function ($q, $http) {
        this.postRequest = function (value, url) {
            var defer = $q.defer();
            $http.post(url, value)
                .success(function (data, status) {
                    defer.resolve(data);
                })
                .error(function (data, status) {
                    defer.reject(data);
                    console.log("post request error");
                });
            return defer.promise;
        };

        this.getRequest = function (value, url) {
            var defer = $q.defer();
            $http.get(url, value)
                .success(function (data, status) {
                    defer.resolve(data);
                })
                .error(function (data, status) {
                    defer.reject(data);
                    console.log("get request error");
                });
            return defer.promise;
        };
    }]);