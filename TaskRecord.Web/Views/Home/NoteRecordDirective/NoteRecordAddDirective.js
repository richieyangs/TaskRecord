﻿angular.module("taskRecordApp")
    .directive("noteRecordAdd", [
        "requestService", "editConditionService", "bindCorrectOptionService", function (requestService, editConditionService, bindCorrectOptionService) {

            return {
                restrict: "E",
                templateUrl: "Views/Home/NoteRecordDirective/NoteRecordAddDirectiveTemplate.html",
                scope: {
                    taskRecordsResource: "=",
                    noteRecords:"=",
                    newNoteRecord: "=",
                    saving: "=",
                    canEdit:"="


                },
                link: function (scope, elment, attrs, ctrl) {

                    scope.editing = function (note,input) {
                        if (scope.canEdit) {
                            note[input] = true;
                        }
                    };

                    scope.addNoteRecord = function () {
                        var resources = scope.taskRecordsResource.apiLinks;

                        if (!scope.newNoteRecord.content || !scope.canEdit) {
                            return;
                        }
                        scope.saving = true;
                        requestService.postRequest(scope.newNoteRecord, resources.addNoteRecord)
                            .then(function(data) {
                                scope.taskRecordsResource = data;
                                bindCorrectOptionService.bind(scope.taskRecordsResource);
                                scope.saving = false;
                                
                            });
                    }
                    scope.editNoteRecord = function (record) {
                        var resources = scope.taskRecordsResource.apiLinks;

                        if (!record.content || !editConditionService.canEdit(scope.taskRecordsResource.queryDate)) {
                            return;
                        }
                        scope.saving = true;
                        requestService.postRequest(record, resources.editNoteRecord)
                            .then(function(data) {
                                scope.taskRecordsResource = data;
                                bindCorrectOptionService.bind(scope.taskRecordsResource);
                                scope.saving = false;
                            });
                    }
                }
            };
        }
    ]);