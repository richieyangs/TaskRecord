﻿angular.module("taskRecordApp")
    .service("editConditionService", [
        function() {
            this.canEdit = function (dateString) {
                var queryDate = new Date(dateString);
                if (queryDate.setHours(0, 0, 0, 0) === new Date().setHours(0, 0, 0, 0)) {
                    return true;
                }
                return false;
            }
        }
    ]);