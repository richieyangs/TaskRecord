using System;

namespace TaskRecord.Web.Views.Home.Api
{
    public class TaskRecordsQuery
    {
        public DateTime QueryDate { get; set; }
    }
}