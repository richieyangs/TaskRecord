﻿angular.module("taskRecordApp")
    .provider("apiNavigator",
        function () {
            this.setApiHome = function(apiHome) {
                this.apiHome = apiHome;
            };
            this.apiHome = "";
            this.$get = ["$q","requestService",function ($q,requestService) {
                var that = this;
                return {
                    apiHome: that.apiHome,
                    getHomeResource: function () {
                        var defer = $q.defer();
                        requestService.getRequest(null, that.apiHome)
                            .then(function(resource) {
                                defer.resolve(resource);
                            });
                        return defer.promise;
                    }
            }
            }];  
        }
    );