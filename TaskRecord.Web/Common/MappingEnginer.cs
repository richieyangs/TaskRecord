﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using AutoMapper.Mappers;

namespace TaskRecord.Web.Common
{
    public class MappingEnginer
    {
        public static IMappingEngine BuildEngine(Action<SafeConfiguration> configureMappings)
        {
            return new MappingEngine(BuildConfiguration(configureMappings));
        }

        private static IConfigurationProvider BuildConfiguration(Action<SafeConfiguration> configureMappings)
        {
            var configuration = new ConfigurationStore(new TypeMapFactory(), MapperRegistry.Mappers);
            var config = new SafeConfiguration(configuration);

            configureMappings(config);
            configuration.AssertConfigurationIsValid();

            return configuration;
        }
    }
}