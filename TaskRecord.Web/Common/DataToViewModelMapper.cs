﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using TaskRecord.Web.EntityFramework;
using TaskRecord.Web.Views.Home;

namespace TaskRecord.Web.Common
{
        public static class DataToViewModelMapper
        {
            private static IMappingEngine _engine;

            static DataToViewModelMapper()
            {
                _engine = MappingEnginer.BuildEngine(configuration =>
                {
                    configuration.CreateMap<EntityFramework.TaskRecord, TaskRecordViewModel>()
                        .ForMember(d => d.Team, s => s.MapFrom(m => m.Team.ToViewModel()))
                        .ForMember(d => d.Source, s => s.MapFrom(m => m.Source.ToViewModel()))
                        .ForMember(d => d.Status, s => s.MapFrom(m => m.Status.ToViewModel()))
                        ;

                    configuration.CreateMap<TeamRecord, TeamRecordViewModel>();

                    configuration.CreateMap<NoteRecord, NoteRecordViewModel>();

                    configuration.CreateMap<TaskRecordTeam, TaskRecordTeamViewModel>();
                    configuration.CreateMap<TaskRecordStatus, TaskRecordStatusViewModel>();
                    configuration.CreateMap<TaskRecordSource, TaskRecordSourceViewModel>();

                });
            }

            public static List<TaskRecordViewModel> ToViewModels(this IEnumerable<EntityFramework.TaskRecord> taskRecords)
            {
                if (taskRecords != null)
                {
                    var records = taskRecords.Select(x => _engine.Map<TaskRecordViewModel>(x));
                    return records.ToList();
                }
               
                return new List<TaskRecordViewModel>();
            }

            public static TeamRecordViewModel ToViewModel(this TeamRecord teamRecord)
            {
                if (teamRecord != null)
                {
                    return _engine.Map<TeamRecordViewModel>(teamRecord);
                }
               return new TeamRecordViewModel();
            }

            public static List<NoteRecordViewModel> ToViewModels(this IEnumerable<NoteRecord> noteRecords)
            {
                if (noteRecords != null)
                {
                    var records = noteRecords.Select(x => _engine.Map<NoteRecordViewModel>(x));
                    return records.ToList();
                }
                return new List<NoteRecordViewModel>();
            }

            public static TaskRecordTeamViewModel ToViewModel(this TaskRecordTeam team)
            {
                return _engine.Map<TaskRecordTeamViewModel>(team);
            }

            public static TaskRecordSourceViewModel ToViewModel(this TaskRecordSource source)
            {
                return _engine.Map<TaskRecordSourceViewModel>(source);
            }

            public static TaskRecordStatusViewModel ToViewModel(this TaskRecordStatus status)
            {
                return _engine.Map<TaskRecordStatusViewModel>(status);
            }
        }
}