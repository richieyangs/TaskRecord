﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using log4net;
using TaskRecord.Web.Common;
using TaskRecord.Web.Dal;
using TaskRecord.Web.EntityFramework;
using TaskRecord.Web.Views.Home;
using WebApiContrib.IoC.CastleWindsor;

namespace TaskRecord.Web
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            var container = ApplicationBootstrap.SetupContainer();

            //**********************web api***************************
            GlobalConfiguration.Configure(WebApiConfig.Register);
            GlobalConfiguration.Configuration.DependencyResolver = new WindsorResolver(container);
            //**********************web api***************************



            //*********************mvc start*******************
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            var factor = new ControllerFactory<HomeController>(container.Kernel);
            ControllerBuilder.Current.SetControllerFactory(factor);
            //**********************mvc end********************




            //TODO:dangerous! only used for development
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<TaskRecordDbContext>());
            DictionaryDataOperator.AddIfNotExsist();
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var _log = LogManager.GetLogger(typeof(WebApiApplication));
            _log.Error("Unhandled exception", Server.GetLastError().GetBaseException());
        }
    }
}
