﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using TaskRecord.Web.EntityFramework;

namespace TaskRecord.Web.Dal
{
    public class NoteRecordDal:IDal
    {
        public List<NoteRecord> GetNoteRecords(DateTime queryDate)
        {

            var queryTomorrow = queryDate.Date.AddDays(1);
            var queryYestoday = queryDate.Date;
            using (var db = new TaskRecordDbContext())
            {
                var records = db.Notes.Where(x => (x.Date >= queryYestoday && x.Date <= queryTomorrow)).ToList();
                return records;
            }
        }

        public void EditNoteRecord(NoteRecord noteRecord)
        {
            using (var db = new TaskRecordDbContext())
            {
                var record = db.Notes.Single(x => x.Id == noteRecord.Id);
                record.Content = noteRecord.Content;
                db.Entry(record).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void AddNoteRecord(NoteRecord noteRecord)
        {
            noteRecord.Date = DateTime.Now;
            using (var db = new TaskRecordDbContext())
            {
                db.Notes.Add(noteRecord);
                db.SaveChanges();
            }
        }
    }
}